#made by Naomi Sinsheimer
#updated 6/1/2020


#Prompt user for Store Number 
StoreNumbers = input("Input store numbers separated by a single space > ")

#Create a list and add all the store numbers as elements
StoreList = StoreNumbers.split(" ")
StoreList = list(map(int, StoreList))
RouteList = []

#Subnet Mask and the end of the IP address, as it was always .0
subnetMask = ".0 mask 255.255.255.0 192.168.3.80 metric 5"

for x in StoreList:
    if x <= 254:
        RouteList.append("route delete -p 10.11."+str(x) + subnetMask)
        RouteList.append("route delete -p 10.111."+str(x) + subnetMask)
        RouteList.append("route delete -p 10.211."+str(x) + subnetMask)
    elif x >= 255 and x <= 508:
        x = x - 254
        RouteList.append("route delete -p 10.12."+str(x) + subnetMask)
        RouteList.append("route delete -p 10.112."+str(x) + subnetMask)
        RouteList.append("route delete -p 10.212."+str(x) + subnetMask)
    elif x >= 509 and x <= 762:
        x = x - 508
        RouteList.append("route delete -p 10.13."+str(x) + subnetMask)
        RouteList.append("route delete -p 10.113."+str(x) + subnetMask)
        RouteList.append("route delete -p 10.213."+str(x) + subnetMask)

#Create a .bat file with the commands that's ready to run
with open('bye_bye_routes.bat', 'w+') as f:
    for item in RouteList:
        f.write("%s\n" % item)